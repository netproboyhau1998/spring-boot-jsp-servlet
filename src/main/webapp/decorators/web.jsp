<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Trang chủ</title>
<link href="<c:url value='/web/bootstrap/css/bootstrap.min.css' />" rel="stylesheet" type="text/css" media="all"/>
<link href="<c:url value='/web/css/style.css' />" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>
	<%@ include file = "/common/web/header.jsp" %>
	<div class="container">
		<sitemesh:write property="body"/>
	</div>
	<%@ include file = "/common/web/footer.jsp" %>
	<script type="text/javascript" src="<c:url value='/web/jquery/jquery.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/web/bootstrap/js/bootstrap.bundle.min.js' />"></script>
</body>
</html>