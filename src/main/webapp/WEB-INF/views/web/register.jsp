<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
</head>
<body>
	<div class="container">
		<h1 class="form-heading">Register Form</h1>
		<div class="login-form">
			<div class="main-div">
				<c:if test="${message != null}">
					<div class="alert alert-danger">	
							${message}
					</div>
				</c:if>
				<div class="panel">
					<h2>Admin Register</h2>
					<p>Please enter your information</p>
				</div>
				<form action='<c:url value = "/dang-ki"/>' id="formRegister" method="post">
					<div class="form-group">
						<input type="text" class="form-control" id="userName"
							name="userName" placeholder="Username" value="${model.userName}">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="password"
							name="password" placeholder="Password" value="${model.password}">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="fullname"
							name="fullName" placeholder="Fullname" value="${model.fullName}">
					</div>
					<button type="submit" class="btn btn-primary">Register</button>
				</form>
			</div>
			<p class="botto-text">Designed by Hau</p>
		</div>
	</div>
</body>
</html>