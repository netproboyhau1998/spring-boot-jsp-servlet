<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
</head>
<body>
	<div class="container">
		<h1 class="form-heading">Login Form</h1>
		<div class="login-form">
			<div class="main-div">
				<c:if test="${param.incorrectAccount != null}">
					<div class="alert alert-danger">	
							Username or password incorrect
					</div>
				</c:if>
				<c:if test="${param.accessDenied != null}">
					<div class="alert alert-danger">	
							You are not authorize
					</div>
				</c:if>
				<div class="panel">
					<h2>Admin Login</h2>
					<p>Please enter your email and password</p>
				</div>
				<form action="/dang-nhap" id="formLogin"
					method="post">
					<div class="form-group">
						<input type="text" class="form-control" id="userName"
							name="j_username" placeholder="Username">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="password"
							name="j_password" placeholder="Password">
					</div>
                    <div class="form-check">
					     <input class="form-check-input" type="checkbox" name="remember-me" id="remember-me">
					     <label class="form-check-label" for="remember-me">Remember Me</label>
					</div>
					<div class="forgot">
						<a href="reset.html">Forgot password?</a>
					</div>
					<button type="submit" class="btn btn-primary">Login</button>
				</form>
			</div>
			<p class="botto-text">Designed by Hau</p>
		</div>
	</div>
</body>
</html>