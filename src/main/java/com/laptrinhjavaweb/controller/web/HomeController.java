package com.laptrinhjavaweb.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller(value = "homeControllerOfWeb")
public class HomeController {
	
	@RequestMapping(value = {"/", "/trang-chu"}, method = RequestMethod.GET)
	public String homePage() {
		return "web/home";
	}

	@RequestMapping(value = "/dang-nhap", method = RequestMethod.GET)
	public String loginPage() {
		return "web/login";
	}

	@RequestMapping(value = "/dang-ki", method = RequestMethod.GET)
	public String registerPage() {
		return "web/register";
	}
}
