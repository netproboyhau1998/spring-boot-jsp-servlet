package com.laptrinhjavaweb.service;

import com.laptrinhjavaweb.dto.UserDTO;
import com.laptrinhjavaweb.entity.UserEntity;

public interface IUserService {
	public void saveUser(UserDTO userDTO);
	
	UserEntity findOneByUserNameAndStatus(String username, Integer status);
}
