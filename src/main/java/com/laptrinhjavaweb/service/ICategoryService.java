package com.laptrinhjavaweb.service;

import java.util.List;
import java.util.Map;

import com.laptrinhjavaweb.dto.CategoryDTO;
import com.laptrinhjavaweb.entity.CategoryEntity;

public interface ICategoryService {
//	Map<String, String> findAll(); //sử dụng edit.jsp
	List<CategoryDTO> findAll();// sử dụng edit2.jsp
}
