package com.laptrinhjavaweb.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.laptrinhjavaweb.dto.UserDTO;
import com.laptrinhjavaweb.entity.UserEntity;
import com.laptrinhjavaweb.repository.IUserRepository;
import com.laptrinhjavaweb.service.IUserService;

@Service
public class UserService implements IUserService{

	@Autowired
	private IUserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public void saveUser(UserDTO userDTO) {
		UserEntity entity = new UserEntity();
		entity.setUserName(userDTO.getUserName());
		entity.setFullName(userDTO.getFullName());
		entity.setPassword(passwordEncoder.encode(userDTO.getPassword()));
		entity.setStatus(1);
		userRepository.save(entity);
	}

	@Override
	public UserEntity findOneByUserNameAndStatus(String username, Integer status) {
		return userRepository.findOneByUserNameAndStatus(username, status);
	}
}
