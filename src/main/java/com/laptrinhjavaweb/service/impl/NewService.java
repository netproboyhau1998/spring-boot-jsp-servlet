package com.laptrinhjavaweb.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.laptrinhjavaweb.converter.NewConverter;
import com.laptrinhjavaweb.dto.NewDTO;
import com.laptrinhjavaweb.entity.CategoryEntity;
import com.laptrinhjavaweb.entity.NewEntity;
import com.laptrinhjavaweb.repository.ICategoryRepository;
import com.laptrinhjavaweb.repository.INewRepository;
import com.laptrinhjavaweb.service.INewService;

@Service
public class NewService implements INewService {

	@Autowired
	private ICategoryRepository categoryRepository;
	@Autowired
	private INewRepository newRepository;
	@Autowired
	private NewConverter converter;

	@Override
	@Transactional
	public NewDTO save(NewDTO newDTO) {
		NewEntity newEntity = new NewEntity();
		if (newDTO.getId() != null) {
			NewEntity oldNewEntity = newRepository.findOne(newDTO.getId());
			newEntity = converter.toEntity(oldNewEntity, newDTO);
		}else {
			newEntity = converter.toEntity(newDTO);
		}
		CategoryEntity categoryEntity = categoryRepository.findOneByCode(newDTO.getCategoryCode());
		newEntity.setCategory(categoryEntity);
		newRepository.save(newEntity);
		return converter.toDto(newEntity);
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids) {
			newRepository.delete(id);
		}
	}

	@Override
	public List<NewDTO> findAll(Pageable pageable) {
		List<NewDTO> newDTOs = new ArrayList<NewDTO>();
		List<NewEntity> newEntities = newRepository.findAll(pageable).getContent();
		for(NewEntity entity : newEntities) {
			NewDTO newDTO = converter.toDto(entity);
			newDTOs.add(newDTO);
		}
		return newDTOs;
	}

	@Override
	public int totalItem() {
		return (int) newRepository.count();
	}

}
