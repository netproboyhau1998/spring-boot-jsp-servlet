package com.laptrinhjavaweb.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.laptrinhjavaweb.converter.CategoryConverter;
import com.laptrinhjavaweb.dto.CategoryDTO;
import com.laptrinhjavaweb.entity.CategoryEntity;
import com.laptrinhjavaweb.repository.ICategoryRepository;
import com.laptrinhjavaweb.service.ICategoryService;

@Service
public class CategoryService implements ICategoryService {

	@Autowired
	private ICategoryRepository categoryRepository;
	
	@Autowired
	private CategoryConverter converter;

	@Override
	public List<CategoryDTO> findAll() {
		List<CategoryEntity> categoryEntities = categoryRepository.findAll();
		List<CategoryDTO> result = new ArrayList<CategoryDTO>();
		for(CategoryEntity entity : categoryEntities) {
			CategoryDTO categoryDTO = converter.toDto(entity);
			result.add(categoryDTO);
		}
		return result;
	}
	
	
//	@Override
//	public Map<String, String> findAll() {
//		List<CategoryEntity> categoryEntities = categoryRepository.findAll();
//		Map<String, String> result = new HashMap<String, String>();
//		for(CategoryEntity entity : categoryEntities) {
//			result.put(entity.getCode(), entity.getName());
//		}
//		return result;
//	}
	
}
