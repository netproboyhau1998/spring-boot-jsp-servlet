package com.laptrinhjavaweb.config;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;

public class CustomSiteMeshFilter extends ConfigurableSiteMeshFilter {
	
	 @Override
     protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
         builder.addDecoratorPath("/*", "/decorators/web.jsp")
         .addDecoratorPath("/quan-tri/*", "/decorators/admin.jsp")
         .addDecoratorPath("/dang-nhap", "/decorators/login.jsp")
         .addDecoratorPath("/dang-ki", "/decorators/register.jsp")
         .addExcludedPath("/api/*");
     }
}
