package com.laptrinhjavaweb.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.laptrinhjavaweb.security.CustomAccessDeniedHandler;
import com.laptrinhjavaweb.security.CustomFailureHandler;
import com.laptrinhjavaweb.security.CustomLogoutSuccessHandler;
import com.laptrinhjavaweb.security.CustomSuccessHandler;
import com.laptrinhjavaweb.service.impl.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	@Bean
	protected UserDetailsService userDetailsService() {
		return new UserDetailsServiceImpl();
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}
	
	@Bean
	public SimpleUrlAuthenticationSuccessHandler customSuccessHandler() {
		return new CustomSuccessHandler();
	}
	
	@Bean
	public SimpleUrlAuthenticationFailureHandler customFailureHandler() {
		return new CustomFailureHandler();
	}
	
	@Bean
	public SimpleUrlLogoutSuccessHandler customLogoutSuccessHandler() {
		return new CustomLogoutSuccessHandler();
	}

	@Bean
	public AccessDeniedHandler customAccessDeniedHandler() {
		return new CustomAccessDeniedHandler();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		 auth.authenticationProvider(authenticationProvider());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		 // Cấu hình remember me, thời gian là 1296000 giây
	    http.rememberMe().key("uniqueAndSecret").tokenValiditySeconds(1296000);
	    
		http
	        .authorizeRequests()
	        .antMatchers("/trang-chu/**")
            .hasAnyAuthority("USER", "ADMIN")
            .antMatchers("/quan-tri/**")
            .hasAuthority("ADMIN")// sài hasRole khi role bắt đầu bằng "ROLE_", ngược lại thì sài hasAuthority
	        .anyRequest()
	        .permitAll()
	        .and()
	        .exceptionHandling()
	        .accessDeniedHandler(customAccessDeniedHandler())
	        .and()
	        .formLogin()
	        .loginPage("/dang-nhap")
	        .permitAll()
	        .successHandler(customSuccessHandler())
	        .failureHandler(customFailureHandler())
	        .passwordParameter("j_password")
	        .usernameParameter("j_username")
	        .and()
	        .logout()
	        .logoutUrl("/thoat")
	        .logoutRequestMatcher(new AntPathRequestMatcher("/thoat", "GET"))
	        .clearAuthentication(true)
	        .invalidateHttpSession(true)
	        .deleteCookies("JSESSIONID")
	        .logoutSuccessHandler(customLogoutSuccessHandler())
	        .and()
	        .csrf()
	        .disable();
	}
}
