package com.laptrinhjavaweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class Application extends SpringBootServletInitializer {

	// Phương thức configure được ghi đè trong class kế thừa
	// SpringBootServletInitializer có tác dụng khi bạn muốn triển khai ứng dụng
	// Spring Boot dưới dạng WAR và chạy trên một máy chủ servlet (ví dụ: Tomcat
	// ngoại vi) thay vì chạy dưới dạng JAR nhúng.
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
