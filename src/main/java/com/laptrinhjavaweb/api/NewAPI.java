package com.laptrinhjavaweb.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.laptrinhjavaweb.dto.NewDTO;
import com.laptrinhjavaweb.service.INewService;

@RestController
@RequestMapping(value = "/api")
public class NewAPI {

	@Autowired
	private INewService newService;

	@GetMapping(value = "/new")
	@ResponseBody
	public NewDTO showListNew(@RequestParam(name = "page") int page, @RequestParam(name = "limit") int limit) {
		NewDTO newDTO = new NewDTO();
		newDTO.setPage(page);
		Pageable pageable = new PageRequest(page - 1, limit);
		newDTO.setListResult(newService.findAll(pageable));
		int totalPage = (int) Math.ceil((double) newService.totalItem() / limit);
		newDTO.setTotalPage(totalPage);
		return newDTO;
	}

	@PostMapping(value = "/new")
	public NewDTO createNew(@RequestBody NewDTO model) {
		return newService.save(model);
	}

	@PutMapping(value = "/new")
	public NewDTO updateNew(@RequestBody NewDTO model) {
		return newService.save(model);
	}

	@DeleteMapping(value = "/new")
	public void deleteNew(@RequestBody Long[] ids) {
		newService.delete(ids);
	}
}
